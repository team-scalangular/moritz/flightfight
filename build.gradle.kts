import net.minecrell.pluginyml.bukkit.BukkitPluginDescription

plugins {
    id("java")
    id("maven-publish")
    id("net.minecrell.plugin-yml.bukkit") version "0.5.0"
}

repositories {
    mavenCentral()
    maven("https://hub.spigotmc.org/nexus/content/repositories/public/")
    maven("https://oss.sonatype.org/content/repositories/central")
    maven("https://jitpack.io")
}


dependencies {
    compileOnly("org.spigotmc:spigot-api:1.17-R0.1-SNAPSHOT")
    testImplementation("com.github.seeseemelk:MockBukkit-v1.14:0.2.2")
    testImplementation("org.mockito:mockito-core:4.0.0")
}

val pluginVersion = "1.1.0"
group = "com.scalangular.moritz"
version = pluginVersion

bukkit {
    main = "com.gitlab.linde9821.TridentFlightFight.Plugin.TridentFlightFightPlugin"
    apiVersion = "1.17"
    version = pluginVersion

    load = BukkitPluginDescription.PluginLoadOrder.STARTUP

    authors = listOf("Moritz Lindner")

    commands {
        register("tridentFlightFight") {
            description = "This is the basecommand for tridentFlightFight"
            usage = "/<command>"
            permission = "<plugin name>.basic"
            permissionMessage = "You don't have <permission>"
        }
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
        }
    }
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}
