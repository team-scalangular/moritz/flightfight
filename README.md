# TridentFlightFight 1.1.0

## Description

This plugin provides a gamemode where you will get an Elytra, Rockets and some tridents. Then you can try to hit other player with it which is a lot of fun. I will provide a video which shows some gameplay soon.

you can request changes/features here

If you discover any issues or want to request changes/features please report them [here](https://gitlab.com/linde9821/flightfight/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)

## Implemented Commands

Code (Text):
/TridentFlightFight location
defines the location the gamemode starts. When a player is added or removed he will be teleported to this location

Code (Text):
/TridentFlightFight add <player>
Adds a player to the gamemode

Code (Text):
/TridentFlightFight remove <player>
Removes a player from the gamemode

## Features
add Players to the TridentFlightFight
remove Players from the TridentFlightFight
set a spawn for the TridentFlightFight
inventory restoring
settingssaving
Planned Features
wheater change to use the elytra with a trident and riptide
high scores and score tables
notifications when something happens
support for more languages
Inventory customization
gamemodesession
