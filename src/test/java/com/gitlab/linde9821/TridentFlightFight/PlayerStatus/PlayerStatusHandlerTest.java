package com.gitlab.linde9821.TridentFlightFight.PlayerStatus;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.entity.PlayerMock;
import com.gitlab.linde9821.TridentFlightFight.Plugin.TridentFlightFightPlugin;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PlayerStatusHandlerTest {
    private ServerMock server;
    private TridentFlightFightPlugin plugin;
    private PlayerStatusHandler playerStatusHandler;

    @Before
    public void setUp() {
        server = MockBukkit.mock();
        plugin = MockBukkit.load(TridentFlightFightPlugin.class);
        server.addSimpleWorld("world");

        this.playerStatusHandler = new PlayerStatusHandler();
    }

    @After
    public void tearDown() {
        MockBukkit.unload();
    }

    @Test
    public void storePlayerDate_DefaultValues_CorrectlySave() {
        PlayerMock player = server.addPlayer();
        assertTrue(this.playerStatusHandler.storePlayerDate(player));
    }

    @Test
    public void storePlayerDate_NullPLayer_NotSaved() {
        PlayerMock player = null;
        assertFalse(this.playerStatusHandler.storePlayerDate(player));
    }

}