package com.gitlab.linde9821.TridentFlightFight.CommandExecuter;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import com.gitlab.linde9821.TridentFlightFight.Plugin.TridentFlightFightPlugin;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CommandHandlerTest {
    private ServerMock server;
    private CommandHandler commandHandler  ;
    private TridentFlightFightPlugin plugin;

    @Before
    public void setUp() {
        server = MockBukkit.mock();
        plugin = MockBukkit.load(TridentFlightFightPlugin.class);
        commandHandler = new CommandHandler(plugin);
    }

    @Test
    public void chooseExecutor_Add_AddChosen() {
        String command = "add";
        CommandHandler.Executors expected = CommandHandler.Executors.Add;

        assertEquals(expected, commandHandler.selectCommandExecutor(command));
    }

    @Test
    public void chooseExecutor_Remove_RemoveChosen() {
        String command = "remove";
        CommandHandler.Executors expected = CommandHandler.Executors.Remove;

        assertEquals(expected, commandHandler.selectCommandExecutor(command));
    }

    @Test
    public void chooseExecutor_Location_LocationChosen() {
        String command = "location";
        CommandHandler.Executors expected = CommandHandler.Executors.Location;

        assertEquals(expected, commandHandler.selectCommandExecutor(command));
    }

    @Test
    public void chooseExecutor_UnknownCommand_UnknownChosen() {
        String command = "test";
        CommandHandler.Executors expected = CommandHandler.Executors.Unknown;

        assertEquals(expected, commandHandler.selectCommandExecutor(command));
    }

    @Test
    public void chooseExecutor_NullCommand_UnknownChosen() {
        String command = null;
        CommandHandler.Executors expected = CommandHandler.Executors.Unknown;

        assertEquals(expected, commandHandler.selectCommandExecutor(command));
    }

    @After
    public void tearDown() {
        MockBukkit.unload();
    }
}