package com.gitlab.linde9821.TridentFlightFight.Config;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import com.gitlab.linde9821.TridentFlightFight.Plugin.TridentFlightFightPlugin;
import org.bukkit.Location;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ConfigHandlerTest {

    private ServerMock server;
    private TridentFlightFightPlugin plugin;

    @Before
    public void setUp() {
        server = MockBukkit.mock();
        plugin = MockBukkit.load(TridentFlightFightPlugin.class);
        server.addSimpleWorld("world");
    }

    @Test
    public void decodeLocation_defaultValues_CorrectLocation() {
        ConfigHandler configHandler = new ConfigHandler(plugin);
        Location expectedLocation = new Location(server.getWorld("world"), 18d, 127d, 13.8d);
        assertEquals(expectedLocation, configHandler.decodeLocation("world;18.0;127.0;13.8"));
    }

    @Test
    public void decodeLocation_unknownWorld_Null() {
        ConfigHandler configHandler = new ConfigHandler(plugin);
        assertNull(configHandler.decodeLocation("wrld;18.0;127.0;13.8"));
    }

    @Test
    public void encodeLocation_unknownWorld_CorrectLocation() {
        ConfigHandler configHandler = new ConfigHandler(plugin);
        Location locationToEncode = new Location(server.getWorld("world"), 18d, 127d, 13.8d);
        assertEquals("world;18.0;127.0;13.8", configHandler.encodeLocation(locationToEncode));
    }

    @Test
    public void saveSpawnLocation() {
        ConfigHandler configHandler = new ConfigHandler(plugin);
        Location locationToEncode = new Location(server.getWorld("world"), 18d, 127d, 13.8d);

        configHandler.saveSpawnLocation(locationToEncode);
    }

    @Test
    public void loadSpawnLocation() {
        ConfigHandler configHandler = new ConfigHandler(plugin);
        Location locationToEncode = new Location(server.getWorld("world"), 18d, 127d, 13.8d);

        configHandler.saveSpawnLocation(locationToEncode);

        assertEquals(locationToEncode, configHandler.loadSpawnLocation());
    }

    @Test
    public void loadSpawnLocation_NothingSaved() {
        ConfigHandler configHandler = new ConfigHandler(plugin);

        assertNull(configHandler.loadSpawnLocation());
    }

    @After
    public void tearDown() {
        MockBukkit.unload();
    }
}