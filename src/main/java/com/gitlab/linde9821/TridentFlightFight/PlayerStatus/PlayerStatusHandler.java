package com.gitlab.linde9821.TridentFlightFight.PlayerStatus;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class PlayerStatusHandler {
    private final HashMap<String, PlayerStatusHolder> playersStatus;

    public PlayerStatusHandler() {
        this.playersStatus = new HashMap<>();
    }

    public boolean storePlayerDate(Player playerToStoreData) {
        if (playerToStoreData == null) {
            return false;
        }

        playersStatus.put(playerToStoreData.getDisplayName(), new PlayerStatusHolder(playerToStoreData));
        return true;
    }

    public void restorePlayerStatusToPlayer(Player playerToRestore) {
        if (playerToRestore != null) {
            PlayerStatusHolder playerStatusHolderToRestore = playersStatus.get(playerToRestore.getDisplayName());

            playerToRestore.setGameMode(GameMode.valueOf(playerStatusHolderToRestore.getPlayerGameMode()));
            playerToRestore.getInventory().setContents(playerStatusHolderToRestore.getInventory());
            playerToRestore.getInventory().setArmorContents(playerStatusHolderToRestore.getArmorMap());
            playerToRestore.teleport(playerStatusHolderToRestore.getTeleportLocation());

            playersStatus.remove(playerToRestore.getDisplayName());
        }
    }
}
