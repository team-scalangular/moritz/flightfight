package com.gitlab.linde9821.TridentFlightFight.PlayerStatus;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class PlayerStatusHolder {

    private final ItemStack[] inventory;
    private final ItemStack[] armorMap;

    private final String playerGameMode;
    private final Location teleportLocation;

    public PlayerStatusHolder(Player player) {
        this.playerGameMode = player.getGameMode().name();
        this.teleportLocation = player.getLocation().clone();

        this.inventory = player.getInventory().getContents().clone();
        this.armorMap = player.getInventory().getArmorContents().clone();
    }

    public ItemStack[] getInventory() {
        return inventory;
    }

    public ItemStack[] getArmorMap() {
        return armorMap;
    }

    public String getPlayerGameMode() {
        return playerGameMode;
    }

    public Location getTeleportLocation() {
        return teleportLocation;
    }
}
