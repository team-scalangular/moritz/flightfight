package com.gitlab.linde9821.TridentFlightFight.Config;

import org.bukkit.Location;

public class CurrentConfig {
    private Location tridentFlightFightSpawnLocation = null;
    private boolean changeWeatherOnStart = true;

    public Location getTridentFlightFightSpawnLocation() {
        return tridentFlightFightSpawnLocation;
    }

    public void setTridentFlightFightSpawnLocation(Location tridentFlightFightSpawnLocation) {
        this.tridentFlightFightSpawnLocation = tridentFlightFightSpawnLocation;
    }

    public boolean isChangeWeatherOnStart() {
        return changeWeatherOnStart;
    }

    public void setChangeWeatherOnStart(boolean changeWeatherOnStart) {
        this.changeWeatherOnStart = changeWeatherOnStart;
    }
}
