package com.gitlab.linde9821.TridentFlightFight.Config;

import com.gitlab.linde9821.TridentFlightFight.Plugin.TridentFlightFightPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.Objects;

public class ConfigHandler {

    private final String PATH_TO_SPAWN_LOCATION = "config.TridentFlightFightAlpha.spawn";
    private final String PATH_TO_WEATHER_CHANGE = "config.TridentFlightFightAlpha.weather";

    private final TridentFlightFightPlugin plugin;
    private final CurrentConfig currentConfig;

    public ConfigHandler(TridentFlightFightPlugin plugin) {
        this.plugin = plugin;
        this.plugin.saveDefaultConfig();
        this.currentConfig = new CurrentConfig();
    }

    public void saveWeatherChange(boolean changeWeather){
        this.plugin.getConfig().set(PATH_TO_WEATHER_CHANGE, changeWeather);
        this.plugin.saveConfig();
        this.currentConfig.setChangeWeatherOnStart(changeWeather);
    }

    public boolean loadWeatherChange(){
        return currentConfig.isChangeWeatherOnStart();
    }

    public void saveSpawnLocation(Location locationToSave) {
        this.plugin.getConfig().set(PATH_TO_SPAWN_LOCATION, this.encodeLocation(locationToSave));
        this.plugin.saveConfig();
        this.currentConfig.setTridentFlightFightSpawnLocation(locationToSave);
    }

    public Location loadSpawnLocation() {
        if (currentConfig.getTridentFlightFightSpawnLocation() == null) {
            String encodedString = this.plugin.getConfig().getString(PATH_TO_SPAWN_LOCATION);

            if (encodedString == null) {
                return null;
            }

            return this.decodeLocation(encodedString);
        }

        return currentConfig.getTridentFlightFightSpawnLocation();
    }

    public String encodeLocation(Location locationToEncode) {
        double x, y, z;
        String worldName = Objects.requireNonNull(locationToEncode.getWorld()).getName();
        x = locationToEncode.getX();
        y = locationToEncode.getY();
        z = locationToEncode.getZ();

        return String.format("%s;%s;%s;%s", worldName, x, y, z);
    }

    public Location decodeLocation(String decodedLocation) {
        String[] parts = decodedLocation.split(";");

        World world = Bukkit.getWorld(parts[0]);

        if (world == null) {
            return null;
        }

        return new Location(world, Double.parseDouble(parts[1]), Double.parseDouble(parts[2]), Double.parseDouble(parts[3]));
    }
}
