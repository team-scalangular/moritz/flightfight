package com.gitlab.linde9821.TridentFlightFight.GameMode;

import com.gitlab.linde9821.TridentFlightFight.Config.ConfigHandler;
import com.gitlab.linde9821.TridentFlightFight.PlayerStatus.PlayerStatusHandler;
import com.gitlab.linde9821.TridentFlightFight.Plugin.TridentFlightFightPlugin;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.ArrayList;
import java.util.List;

public final class GamemodeHandler implements Listener {
    TridentFlightFightPlugin plugin;
    private final List<Player> playersInGameMode;
    private final PlayerStatusHandler playerStatusHandler;
    private final ConfigHandler configHandler;

    public GamemodeHandler(TridentFlightFightPlugin plugin) {
        this.plugin = plugin;
        this.playerStatusHandler = plugin.getPlayerStatusHandler();
        this.configHandler = plugin.getConfigHandler();
        this.playersInGameMode = new ArrayList<>();

        this.plugin.getServer().getPluginManager().registerEvents(this, this.plugin);
    }

    public boolean addPlayerToGameMode(Player playerToAdd) {
        if (!playersInGameMode.contains(playerToAdd)) {
            Location flightFightSpawnLocation = configHandler.loadSpawnLocation();
            playerStatusHandler.storePlayerDate(playerToAdd);

            playerToAdd.getInventory().clear();
            playerToAdd.setGameMode(GameMode.SURVIVAL);
            this.setPlayerToMaxHealth(playerToAdd);
            this.setPlayerInventoryToGameModeConfiguration(playerToAdd);
            playerToAdd.setCompassTarget(flightFightSpawnLocation);
            playerToAdd.teleport(flightFightSpawnLocation);

            playersInGameMode.add(playerToAdd);
            playerToAdd.sendMessage("Added you to FightFlight");

            return true;
        }

        return false;
    }

    public boolean removePlayerFromGame(Player playerToRemove) {
        if (playersInGameMode.contains(playerToRemove)) {
            playerToRemove.getInventory().clear();
            this.setPlayerToMaxHealth(playerToRemove);
            playerStatusHandler.restorePlayerStatusToPlayer(playerToRemove);
            playersInGameMode.remove(playerToRemove);
            playerToRemove.sendMessage("Removed you from TridentFightFlight");
            return true;
        }

        return false;
    }

    public void removeAll() {
        playersInGameMode.forEach(this::removePlayerFromGame);
    }

    private void setPlayerToMaxHealth(Player playerToSetToMaxHealth) {
        playerToSetToMaxHealth.setHealth(20);
    }

    private void setPlayerInventoryToGameModeConfiguration(Player playerToSetInventory) {
        PlayerInventory playerInventory = playerToSetInventory.getInventory();

        ItemStack rockets = new ItemStack(Material.FIREWORK_ROCKET, 64);
        ItemStack compass = new ItemStack(Material.COMPASS);
        ItemStack elytra = new ItemStack(Material.ELYTRA);
        ItemStack loyaltyTrident = new ItemStack(Material.TRIDENT);
        ItemStack riptideTrident = new ItemStack(Material.TRIDENT);

        loyaltyTrident.addEnchantment(Enchantment.LOYALTY, 3);
        riptideTrident.addEnchantment(Enchantment.RIPTIDE, 3);

        playerInventory.setItemInMainHand(rockets);
        playerInventory.setChestplate(elytra);
        playerInventory.addItem(compass, riptideTrident);
        for (int i = 0; i < 5; i++) {
            playerInventory.addItem(loyaltyTrident);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDeath(PlayerDeathEvent playerDeathEvent) {
        Player killedPlayer = playerDeathEvent.getEntity();

        playerDeathEvent.getDrops().clear();

        if (playersInGameMode.contains(killedPlayer)) {
            removePlayerFromGame(killedPlayer);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerLogout(PlayerQuitEvent playerQuitEvent) {
        Player quitingPlayer = playerQuitEvent.getPlayer();

        if (playersInGameMode.contains(quitingPlayer)) {
            removePlayerFromGame(quitingPlayer);
        }
    }
}
