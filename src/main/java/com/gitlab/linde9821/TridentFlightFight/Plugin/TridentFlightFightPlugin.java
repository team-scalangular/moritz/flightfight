package com.gitlab.linde9821.TridentFlightFight.Plugin;

import com.gitlab.linde9821.TridentFlightFight.CommandExecuter.CommandHandler;
import com.gitlab.linde9821.TridentFlightFight.Config.ConfigHandler;
import com.gitlab.linde9821.TridentFlightFight.GameMode.GamemodeHandler;
import com.gitlab.linde9821.TridentFlightFight.PlayerStatus.PlayerStatusHandler;
import java.util.Objects;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

import java.io.File;

public class TridentFlightFightPlugin extends JavaPlugin {

    private ConfigHandler configHandler;
    private PlayerStatusHandler playerStatusHandler;
    private GamemodeHandler gamemodeHandler;

    public TridentFlightFightPlugin() {
        super();
    }

    protected TridentFlightFightPlugin(JavaPluginLoader loader, PluginDescriptionFile description, File dataFolder, File file) {
        super(loader, description, dataFolder, file);
    }

    public GamemodeHandler getGamemodeHandler() {
        return gamemodeHandler;
    }

    public ConfigHandler getConfigHandler() {
        return configHandler;
    }

    public PlayerStatusHandler getPlayerStatusHandler() {
        return playerStatusHandler;
    }

    @Override
    public void onEnable() {
        this.initialize();
    }

    private void initialize() {
        this.configHandler = new ConfigHandler(this);
        this.playerStatusHandler = new PlayerStatusHandler();
        this.gamemodeHandler = new GamemodeHandler(this);
        CommandHandler commandHandler = new CommandHandler(this);

        Objects.requireNonNull(Bukkit.getPluginCommand("tridentFlightFight")).setExecutor(commandHandler);
    }

    @Override
    public void onDisable() {
        gamemodeHandler.removeAll();
    }
}
