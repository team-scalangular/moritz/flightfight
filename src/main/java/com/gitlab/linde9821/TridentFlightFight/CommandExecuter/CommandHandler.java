package com.gitlab.linde9821.TridentFlightFight.CommandExecuter;

import com.gitlab.linde9821.TridentFlightFight.Plugin.TridentFlightFightPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandHandler implements CommandExecutor {

    TridentFlightFightPlugin plugin;
    private final AddExecutor addExecutor;
    private final RemoveExecutor removeExecutor;
    private final LocationExecutor locationExecutor;

    public CommandHandler(TridentFlightFightPlugin plugin) {
        this.plugin = plugin;
        addExecutor = new AddExecutor(plugin);
        removeExecutor = new RemoveExecutor(plugin);
        locationExecutor = new LocationExecutor(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("tridentFlightFight")) {
            Player player = null;

            if (sender instanceof Player) {
                player = (Player) sender;
            } else {
                sender.sendMessage("this command can only be run by a player");
                return false;
            }

            if (args.length < 1) {
                sender.sendMessage("Please provide a command");
                AbstractExecutor.printUsage(sender);
                return false;
            }

            Executors executorToChoose = this.selectCommandExecutor(args[0]);

            switch (executorToChoose) {
                case Add:
                    addExecutor.execute(sender, command, label, args);
                    break;
                case Remove:
                    removeExecutor.execute(sender, command, label, args);
                    break;
                case Location:
                    locationExecutor.execute(sender, command, label, args);
                    break;
                case Unknown:
                    sender.sendMessage("Unknown command");
                    AbstractExecutor.printUsage(sender);
                    break;
            }
        }
        return false;
    }

    public Executors selectCommandExecutor(String command) {
        if (command == null) {
            return Executors.Unknown;
        }
        if (command.equalsIgnoreCase("location")) {
            return Executors.Location;
        }
        if (command.equalsIgnoreCase("add")) {
            return Executors.Add;
        }

        if (command.equalsIgnoreCase("remove")) {
            return Executors.Remove;
        }

        return Executors.Unknown;
    }

    public enum Executors {
        Location, Add, Remove, Unknown
    }
}
