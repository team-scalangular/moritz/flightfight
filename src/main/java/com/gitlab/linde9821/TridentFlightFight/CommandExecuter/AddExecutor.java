package com.gitlab.linde9821.TridentFlightFight.CommandExecuter;

import com.gitlab.linde9821.TridentFlightFight.Config.ConfigHandler;
import com.gitlab.linde9821.TridentFlightFight.GameMode.GamemodeHandler;
import com.gitlab.linde9821.TridentFlightFight.Plugin.TridentFlightFightPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AddExecutor extends AbstractExecutor {

    GamemodeHandler gamemodeHandler;
    ConfigHandler configHandler;

    public AddExecutor(TridentFlightFightPlugin plugin) {
        super(plugin);
        this.gamemodeHandler = plugin.getGamemodeHandler();
        this.configHandler = plugin.getConfigHandler();
    }

    @Override
    public boolean execute(CommandSender sender, org.bukkit.command.Command command, String label, String[] args) {
        Player commandSenderPlayer = super.isCommandSenderPlayer(sender);

        Location flightFightSpawnLocation = configHandler.loadSpawnLocation();

        if (flightFightSpawnLocation == null) {
            commandSenderPlayer.sendMessage("Before adding a player you first need to set a spawnLocation");
            printUsage(commandSenderPlayer);
            return false;
        }

        if (!checkForArgsLength(2, args)) {
            printUsage(commandSenderPlayer);
            return false;
        }

        Player playerToAdd = Bukkit.getPlayerExact(args[1]);

        if (playerToAdd == null) {
            commandSenderPlayer.sendMessage(String.format("Player %s is not online or unknown", args[1]));
            return false;
        }

        if (gamemodeHandler.addPlayerToGameMode(playerToAdd)) {
            commandSenderPlayer.sendMessage(String.format("Player %s was added to TridentFlightFight", playerToAdd.getDisplayName()));
        } else {
            commandSenderPlayer.sendMessage(String.format("Player %s could not be added because he is already playing the GameMode", playerToAdd.getDisplayName()));
        }
        return true;
    }
}
