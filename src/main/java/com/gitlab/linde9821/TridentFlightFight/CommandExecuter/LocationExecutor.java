package com.gitlab.linde9821.TridentFlightFight.CommandExecuter;

import com.gitlab.linde9821.TridentFlightFight.Config.ConfigHandler;
import com.gitlab.linde9821.TridentFlightFight.Plugin.TridentFlightFightPlugin;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LocationExecutor extends AbstractExecutor {

    private final ConfigHandler configHandler;

    public LocationExecutor(TridentFlightFightPlugin plugin) {
        super(plugin);
        configHandler = plugin.getConfigHandler();
    }

    @Override
    public boolean execute(CommandSender sender, org.bukkit.command.Command command, String label, String[] args) {
        Player commandSenderPlayer = super.isCommandSenderPlayer(sender);

        if (commandSenderPlayer == null) {
            printUsage(commandSenderPlayer);
            return false;
        }

        if (args.length != 1) {
            printUsage(commandSenderPlayer);
            return false;
        }


        Location nextFlightFightSpawnLocation = commandSenderPlayer.getLocation();
        this.configHandler.saveSpawnLocation(nextFlightFightSpawnLocation);

        commandSenderPlayer.sendMessage("The TridentFlightFight spawn was set to your current location");

        return true;
    }

}
