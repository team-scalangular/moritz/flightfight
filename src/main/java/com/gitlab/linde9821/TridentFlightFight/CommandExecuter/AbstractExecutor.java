package com.gitlab.linde9821.TridentFlightFight.CommandExecuter;

import com.gitlab.linde9821.TridentFlightFight.Plugin.TridentFlightFightPlugin;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class AbstractExecutor {

    protected TridentFlightFightPlugin plugin;

    public AbstractExecutor(TridentFlightFightPlugin plugin) {
        this.plugin = plugin;
    }

    static void printUsage(CommandSender sender) {
        sender.sendMessage("Something is wrong with your command!");
        sender.sendMessage("The following Commands are provided:");
        sender.sendMessage("/tridentFLightFight location\n");
        sender.sendMessage("/tridentFLightFight add <player>\n");
        sender.sendMessage("/tridentFLightFight remove <player>\n");
    }

    public abstract boolean execute(CommandSender sender, org.bukkit.command.Command command, String label, String[] args);

    protected Player isCommandSenderPlayer(CommandSender commandSender) {
        if (commandSender instanceof Player) {
            return (Player) commandSender;
        }

        return null;
    }

    protected boolean checkForArgsLength(int expectedLength, String[] args) {
        return args.length == expectedLength;
    }
}
