package com.gitlab.linde9821.TridentFlightFight.CommandExecuter;

import com.gitlab.linde9821.TridentFlightFight.GameMode.GamemodeHandler;
import com.gitlab.linde9821.TridentFlightFight.Plugin.TridentFlightFightPlugin;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RemoveExecutor extends AbstractExecutor {

    private final GamemodeHandler gamemodeHandler;

    public RemoveExecutor(TridentFlightFightPlugin plugin) {
        super(plugin);
        this.gamemodeHandler = plugin.getGamemodeHandler();
    }

    @Override
    public boolean execute(CommandSender sender, org.bukkit.command.Command command, String label, String[] args) {
        Player commandSenderPlayer = super.isCommandSenderPlayer(sender);

        if (!super.checkForArgsLength(2, args)) {
            printUsage(commandSenderPlayer);
            return false;
        }

        Player playerToRemove = Bukkit.getPlayerExact(args[1]);

        if (playerToRemove == null) {
            commandSenderPlayer.sendMessage(String.format("Player %s is not online or unknown", args[1]));
            return false;
        }

        if (gamemodeHandler.removePlayerFromGame(playerToRemove)) {
            commandSenderPlayer.sendMessage(String.format("Player %s was removed from TridentFlightFight", playerToRemove.getDisplayName()));
        } else {
            commandSenderPlayer.sendMessage(String.format("Player %s could not be removed because he is not playing the GameMode", playerToRemove.getDisplayName()));
        }
        return true;
    }
}
